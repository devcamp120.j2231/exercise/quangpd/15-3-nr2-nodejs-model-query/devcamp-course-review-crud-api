//Khai báo thư viện express
const express = require("express");

//Khai báo mongoose
const mongoose = require("mongoose");

//Khai báo model mongoose
const reviewModel = require("./app/models/reviewModel");
const courseModel = require("./app/models/courseModel");

//Khai báo các router
const { courseRouter } = require("./app/routes/courseRouter");
const { reviewRouter } = require("./app/routes/reviewRouter");


 
//Khởi tạo app express
const app = express();



//Khai báo cổng
const port = 8000;

//Khai báo sử dụng json
app.use(express.json()); 

/**** Task 506.10 Application Middleware ******/
const methodMiddleWare =  (req, res, next) => {
    console.log(req.method);

    next();
}

//Kết nối với CSDL (Sử dung mongoose theo phiên bản 6x)
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course",(error) =>{
    if(error) throw error;

    console.log("Connect Successfully");
});


app.use((req, res, next) => { 
    console.log("Time Now Is :");
    console.log(new Date());

    next();
}, methodMiddleWare)

app.use('/get-method', (req, res, next) => {
    console.log('Middleware for all method of an URL');

    next();
})

app.get('/get-method', (req, res, next) => {
    console.log('Middleware for only Get method');

    next();
})

/* 
app.use((req, res, next) => {
    console.log(req.method);

    next();
})*/

app.get('/get-method', (req, res) => {
    console.log('Get-method demo');
    res.json({
        message:'Get-method'
    })
})

app.post('/get-method', (req, res) => {
    console.log('Post-method demo');
    res.json({
        message:'Post-method'
    })
})

app.get('/', (req, res) => {
    var date = new Date();
    console.log('middelware demo');
    res.json({
        message:`Hôm nay là ngày ${date.getDate()} tháng ${date.getMonth()+1} năm ${date.getFullYear()}`
    })
})

/**** Task 506.20 Router Middleware ******/
app.use('/', courseRouter);
app.use('/', reviewRouter);

//Start app tại port 8000
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})