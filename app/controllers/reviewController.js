//Import thư viện mongoose
const mongoose = require("mongoose");

//Import CourseModel và ReviewModel
const courseModel = require("../models/courseModel");
const reviewModel = require("../models/reviewModel");

//get all reviews
const getAllReview = (req,res) =>{
    //B1 : chuẩn bị dữ liệu
    //B2 : kiểm tra dữ liệu
    //B3 : Xử lý và trả về kết quả 
    reviewModel.find((error,data) =>{
        if(error){
            return res.status(500).json({
                message : `Internal server error : ${error.message}`
            })
        }else{
            return res.status(200).json({
                message :"Get All Review Successfully",
                review : data
            })
        }
    })
    console.log("Get All Reviews");

}

//Get a review by id
const getReviewById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let reviewId = req.params.reviewId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(reviewId)){
        return res.status(400).json({
            message : "Review Id không hợp lệ"
        })
    }

    //B3 : Xử lý và trả về kết quả 
    reviewModel.findById(reviewId,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : `Internal server error : ${error.message}`
            })
        }

        if(data){
            return res.status(200).json({
                message : `Get Review By Id Successfully`,
                review : data
            })
        }
        else{
            return res.status(404).json({
                message : `Not Found`
            })
        }
    })
    console.log("Get ReviewId = : " + reviewId);
}

//Create New Review
const createReviewOfCourse = (req,res) =>{
    //B1: chuẩn bị dữ liệu
    const courseId = req.params.courseId;
    var body = req.body;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        return res.status(400).json({
            message : `Bad Request - Course Id is not valid!`
        })
    }

    //Star là số nguyên và lớn hơn 0 nhỏ hơn hoặc bằng 5
    if(body.stars === undefined || !(Number.isInteger(body.stars) && body.stars > 0 && body.stars <= 5)){
        return res.status(400).json({
            message : `Bad request - Star is not valid`
        })
    }

    //B3 : thao tác với cơ sở dữ liệu
    var newReview = {
        _id : mongoose.Types.ObjectId(),
        stars : body.stars,
        note : body.note
    }

    reviewModel.create(newReview ,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }

        //Khi tạo review xong cần thêm id review mới vào mảng review của course
        courseModel.findByIdAndUpdate(courseId, {
            $push : { reviews: data._id}
        }, (err1, data1) =>{
            if(err1){
                return res.status(500).json({
                    message : `Internal server error`
                })
            }else{
                return res.status(201).json({
                    message : `Create review successfully`,
                    data : data
                })
            }

           
        })
    })  
}

//Get all review of course
const getAllReviewOfCourse = (req,res) =>{
    //B1 : thu thập dữ liệu
    const courseId = req.params.courseId;
    //B2 : kiểm tra dữ liệu (cụ thể là kiểm tra cái id gõ trên url xem nó có hợp lệ không)
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        return res.status(400).json({
            message :`Bad Request - CoursesId is not valid`
        })
    }

    //B3 : Xử lý và trả về kết quả
    courseModel.findById(courseId).populate("reviews").exec((err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Get Review ofcourse successfully`,
                reviews : data.reviews
            })
        }
    })
}
//Update a review
const updateReviewById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let reviewId = req.params.reviewId;
    const body = req.body;

    //B2 : kiểm tra dữ liệu 
    if(!mongoose.Types.ObjectId.isValid(reviewId)){
        return res.status(400).json({
            message : `Bad Request - ReviewId không hợp lệ`
        })
    }

    if(body.stars !== undefined && !(Number.isInteger(body.stars) && body.stars > 0 && body.stars <=5)){
        return res.status(400).json({
            message : ` Bad request - Stars(Rate) không hợp lệ`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    const updateReview = {
        stars : body.stars,
        note : body.note
    }
    reviewModel.findByIdAndUpdate(reviewId ,updateReview,(err,data) =>{
        if(err){
           return res.status(500).json({
            message:` Internal server error : ${err.message}`
           })
        }

        if(data){
            return res.status(200).json({
                message : `Update review successfully`,
                reviews : data
            })
        }else{
            return res.status(404).json({
                message :" Not Found"
            })
        }
    })
    
    
}
 
//Delete review by id 
const deleteReviewById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let courseId = req.params.courseId;
    let reviewId = req.params.reviewId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(courseId)){
        return res.status(400).json({
            message : `Bad Request - CourseId không hợp lệ`
        })
    }

    if(!mongoose.Types.ObjectId.isValid(reviewId)){
        return res.status(400).json({
            message: `Bad Request - Review Id không hợp lệ`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    reviewModel.findByIdAndDelete(reviewId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }

        courseModel.findByIdAndUpdate(courseId,{
            $pull :{reviews :reviewId}
        },(err1,data1) =>{
            if(err1){
                return res.status(500).json({
                    message :`Internal server error : ${err1.message}`
                })
            }else{
                return res.status(204).send()
            }
        })
    })
}


//Import thành module 
module.exports = { getAllReview , getReviewById , createReviewOfCourse ,getAllReviewOfCourse, updateReviewById , deleteReviewById};