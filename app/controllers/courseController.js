//import course model
const { default: mongoose } = require('mongoose');
const courseModel = require('../models/courseModel');

//get all courses
const getAllCourses = (req,res) =>{
    console.log("Get all course");
    //B1 : thu thập dữ liệu
    let courseName = req.query.courseName;
    let minStudent = req.query.minStudent;
    let maxStudent = req.query.maxStudent;

    //tạo ra đối tượng lọc
    let condition = {};
    if(courseName) {
        condition.title = courseName;
    }

    if(minStudent){
        condition.noStuden = {$gte : minStudent}
    }

    if(maxStudent){
        condition.noStuden = {...condition.noStuden, $lte : maxStudent};
    }
    console.log(condition);


    //B2 : kiểm tra dữ liệu
    // localhost:8000/courses?courseName=&minStudent=&maxStudent=
    //B3 : Xử lý và trả về kết quả 
    console.log("Get all course");
    courseModel.find(condition,(error,data)=>{
        if(error){
            return res.status(500).json({
                message :`internal server error : ${error.message}`
            })
        }
        else {
            return res.status(201).json({
                message :`Successfully load all data`,
                course : data
            })
        }
    })
    

}

//get a courses
const getCoursesById = (req,res) =>{
    //B1 : Thu thập dữ liệu
    let id = req.params.courseId ;
    console.log("Get CourseId = " + id)
    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : `Id  is invalid`
        })
    }
    //B3 : Xử lý và trả về kết quả 
    courseModel.findById(id,(error,data)=>{
        if(error){
            return res.status(500).json({
                message : `internal server error : ${error.message}`
            })
        }else{
            return res.status(200).json({
                message : `Successfully load data by id`,
                courses : data
            })
        }
    }) 
}

//create new course
const createCourse = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;
    

    //B2 : kiểm tra dữ liệu
    if(!body.title){
        return res.status(400).json({
            message : 'title is required!'
        })
    }

    if(!Number.isInteger(body.noStuden) || body.noStuden < 0){
        return res.status(400).json({
            message :"noStudent is invalid ! "
        })
    }

    //B3 : Xử lý và trả về kết quả 
    let newCourse = new courseModel({
        _id : mongoose.Types.ObjectId(),
        title : body.title,
        description : body.description,
        noStuden : body.noStuden
    });

    courseModel.create(newCourse, (error,data) =>{
        if(error){
            return res.status(500).json({
                message : `Internal server error : ${error.message}`
            });
        }else {
            return res.status(201).json({
                message : `Successfully Created`,
                course : data
            })
        }
    })
    // res.json({
    //     message :"Successfully Created",
    //     newCourse
    // })

    console.log("create a course");
    console.log(body);
}

// update a course
const updateCourse = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.courseId;
    let body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : `Id  is invalid`
        })
    }

    if(!body.title){
        return res.status(400).json({
            message : 'title is required!'
        })
    }

    if(Number.isInteger(body.noStuden) || body.noStuden < 0){
        return res.status(400).json({
            message :"noStudent is invalid ! "
        })
    }

    //B3: xử lý và  trả về kết quả
     let newCourse = new courseModel({
        title : body.title,
        description : body.description,
        noStuden : body.noStuden
     });

     courseModel.findByIdAndUpdate(id,newCourse,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : `internal server error : ${error.message}`
            })
        }else{
            return res.status(201).json({
                message: `Successfully updated !`,
                course : data
            })
        }
     })
}

// delete a course 
const deleteCourse = (req,res) =>{
    //B1 : thu thập dữ liệu
    let id = req.params.courseId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(id)){
        return res.status(400).json({
            message : `Id  is invalid`
        })
    }
    
    //B3 : xử lý và trả về kết quả
    console.log("Delete a course " + id);
    courseModel.findByIdAndDelete(id,(error,data) =>{
        if(error){
            return res.status(500).json({
                message : `Internal server error : ${error.message}`
            })
        } else{
            return res.status(204).json({
                message : `Delete Course Successfully`,
                course : data
            })
        }
        
    })
}
//B3: export thành module
module.exports = { getAllCourses , getCoursesById , createCourse , updateCourse , deleteCourse };