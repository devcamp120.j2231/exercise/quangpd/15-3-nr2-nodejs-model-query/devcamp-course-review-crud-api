// Khai báo thư viện Mongoose
const mongoose = require("mongoose");

// Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

// Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongo
const courseSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    title :{
        type : String,
        required : true,
        unique : true
    },
    description :{
        type : String,
        required : false
    },
    noStuden :{
        type : Number,
        default : 0
    },
    reviews : [
        {
            type : mongoose.Types.ObjectId,
            ref : 'review'
        }
    ]
},{
    timestamps: true
})

// export schema ra model
module.exports = mongoose.model("course",courseSchema);