//B1 : Khai báo thư viện mongoose
const mongoose = require("mongoose");

//Khai báo thư viện Schema của mongoose
const Schema = mongoose.Schema;

//B3 : Tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongo
const reviewSchema = new Schema({
    _id : mongoose.Types.ObjectId,
    stars :{
        type : Number,
        default : 0
    },
    note :{ 
        type : String,
        required : false
    }
},{
    //Tham số tạo ra các trường đặt lịch sử tương tác với bản ghi
    timestamps : true
});
//B4 : export schema ra model

module.exports = mongoose.model("review",reviewSchema);
 