const reviewMiddleWare =  (req, res, next) => {
    console.log("Review Middleware - Time: " + new Date() + " - Method: " + req.method);

    next();
}

const getAllReviewsMiddleWare =  (req, res, next) => {
    console.log("Get All Review Middleware");

    next();
}

const getReviewByIdMiddleWare =  (req, res, next) => {
    console.log("Get A Review By Id Middleware");

    next();
}

const getAllReviewOfCourseMiddleWare = (req,res,next) =>{
    console.log("Get All review Of Course");
    next();
}

const postReviewsMiddleWare =  (req, res, next) => {
    console.log("Create new Review Middleware");

    next();
}

const putReviewByIdMiddleWare =  (req, res, next) => {
    console.log("Update a Review By Id Middleware");

    next();
}

const deleteReviewByIdMiddleWare =  (req, res, next) => {
    console.log("Delete a Review By Id Middleware");

    next();
}

module.exports = {
    reviewMiddleWare,
    getAllReviewsMiddleWare,
    getAllReviewOfCourseMiddleWare,
    getReviewByIdMiddleWare,
    postReviewsMiddleWare,
    putReviewByIdMiddleWare,
    deleteReviewByIdMiddleWare
}