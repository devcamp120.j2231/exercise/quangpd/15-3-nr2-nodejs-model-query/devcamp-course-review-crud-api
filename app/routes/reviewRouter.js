//Khai báo thư viện express
const express = require("express");
const { getAllReview, getReviewById, updateReviewById, deleteReviewById, createReviewOfCourse, getAllReviewOfCourse } = require("../controllers/reviewController");
const { reviewMiddleWare, getAllReviewsMiddleWare, getReviewByIdMiddleWare, putReviewByIdMiddleWare, deleteReviewByIdMiddleWare, postReviewsMiddleWare, getAllReviewOfCourseMiddleWare } = require("../middlewares/reviewMiddleware");

//Khai báo router
const reviewRouter = express.Router();

//Middleware chung cho cả router
reviewRouter.use(reviewMiddleWare);

//Get all review
reviewRouter.get("/reviews", getAllReviewsMiddleWare, getAllReview );

//Get a review by id
reviewRouter.get("/reviews/:reviewId", getReviewByIdMiddleWare, getReviewById);

//Create new review of course
reviewRouter.post("/courses/:courseId/reviews", postReviewsMiddleWare, createReviewOfCourse);

//Get all review of course
reviewRouter.get("/courses/:courseId/reviews",getAllReviewOfCourseMiddleWare,getAllReviewOfCourse);

//Update review by id
reviewRouter.put("/reviews/:reviewId", putReviewByIdMiddleWare, updateReviewById);

//Delete review by id
reviewRouter.delete("/courses/:courseId/reviews/:reviewId", deleteReviewByIdMiddleWare, deleteReviewById);

//Export module
module.exports = { reviewRouter } 