//Khai báo thư viện express
const express = require("express");
//Import controller
const { getAllCourses, getCoursesById, createCourse, updateCourse, deleteCourse } = require("../controllers/courseController");
//import middleware
const { courseMiddleWare, getAllCoursesMiddleWare, getCourseByIdMiddleWare, putCourseByIdMiddleWare, deleteCourseByIdMiddleWare, postCoursesMiddleWare } = require("../middlewares/courseMiddleware");

//Khai báo router
const courseRouter = express.Router();

//Middleware chung cho cả router
courseRouter.use(courseMiddleWare);

//Get all course
courseRouter.get("/courses",getAllCourses );

//Get a course by id
courseRouter.get("/courses/:courseId", getCoursesById);

//Create new course
courseRouter.post("/courses", createCourse);

//Update course by id
courseRouter.put("/courses/:courseId", updateCourse);

//Delete course by id
courseRouter.delete("/courses/:courseId", deleteCourse);

//Export module
module.exports = { courseRouter }